package org.utils;

public abstract class HelperTask
{
	protected static HelperTask instance;
	
	public abstract void prog();
	
	protected void _capture()
	{
		throw new RuntimeException();
	}
	
	public void capture()
	{
		_capture();
	}
	
	protected void _endCapture()
	{
		throw new RuntimeException();
	}
	
	public void endCapture()
	{
		_endCapture();
	}
	
	protected void _validate()
	{
		throw new RuntimeException();
	}
	
	public void validate()
	{
		_validate();
	}
}
