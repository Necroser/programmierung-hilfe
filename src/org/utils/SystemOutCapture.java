package org.utils;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.util.Arrays;
import java.util.List;

public class SystemOutCapture
{
	private ByteArrayOutputStream buffer;
	private PrintStream prev;
	private boolean capturing = false;
	
	public SystemOutCapture()
	{
	}
	
	public void capture()
	{
		prev = System.out;
		
		buffer = new ByteArrayOutputStream();
		
		OutputStream os = new OutputStreamCombiner(Arrays.asList(buffer, prev));
		System.setOut(new PrintStream(os));
		
		capturing = true;
	}
	
	public void endCapture()
	{
		System.setOut(prev);
		prev = null;
		capturing = false;
	}
	
	public String result()
	{
		if (capturing)
			endCapture();
		
		if (buffer == null)
			return "";
		
		return buffer.toString();
	}
	
	private class OutputStreamCombiner extends OutputStream
	{
		
		private List<OutputStream> streams;
		
		public OutputStreamCombiner(List<OutputStream> streams)
		{
			this.streams = streams;
		}

		@Override
		public void write(int arg0) throws IOException
		{
			for (OutputStream os : streams)
				os.write(arg0);
		}
		
		@Override
		public void flush() throws IOException
		{
			for (OutputStream os : streams)
				os.flush();
		}
		
		@Override
		public void close() throws IOException
		{
			for (OutputStream os : streams)
				os.close();
		}
	}
}
