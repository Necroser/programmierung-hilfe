package tasks;

import valid.VTask01;

public class Task01 extends VTask01
{
	
	public static void main(String[] args)
	{
		_main(args);
	}
	
	@Override
	public void prog()
	{
		/*
		 * Input for Program
		 * 	String name1
		 * 	String name2
		 * 	String name3
		 */
		String name1 = "Julius",
				name2 = "Linus",
				name3 = "David";
		
		/*
		 * Task: Show every name in a new line
		 */
		
		// --------------------
		// Writing Space
		
		System.out.println();
		
		// --------------------
	}
	
}
