package tasks;

import valid.VTask03;

public class Task03 extends VTask03
{
	
	public static void main(String[] args)
	{
		_main(args);
	}
	
	@Override
	public void prog()
	{
		/*
		 * Input for Program
		 * 	int basis1
		 * 	int basis2
		 */
		final int basis1 = 7,
			basis2 = 3;
		
		/*
		 * Task: Print the numbers of the session with the base of 7 and 3 from 1 and 51
		 * 
		 * ( Don't separate the sessions )
		 * 
		 * 	e.g.: 	"4"
		 * 			"8"
		 * 			"12"
		 * 			"16"
		 * 			"20"
		 * 
		 */
		
		// --------------------
		// Writing Space
		
		System.out.println();
		
		// --------------------
	}
	
}
