package tasks;

import valid.VTask02;

public class Task02 extends VTask02
{
	
	public static void main(String[] args)
	{
		_main(args);
	}
	
	@Override
	public void prog()
	{
		/*
		 * Input for Program
		 * 	int num1
		 * 	int num2
		 * 	int num3
		 */
		int num1 = 15,
			num2 = 63,
			num3 = 47;
		
		/*
		 * Task: Compare the numbers and print the correct relation
		 * 
		 * 	e.g.: 	"34 < 87"
		 * 			"25 > 3"
		 * 
		 * Schema:
		 * 	num1 <-> num2
		 * 	num1 <-> num3
		 * 	num2 <-> num3
		 */
		
		// --------------------
		// Writing Space
		
		System.out.println();
		
		// --------------------
	}
	
}
