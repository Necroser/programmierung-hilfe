package valid;

import org.utils.HelperTask;
import org.utils.SystemOutCapture;

import tasks.Task01;

public abstract class VTask01 extends HelperTask
{
	private SystemOutCapture buffer;
	
	protected static void _main(String[] args)
	{
		try
		{
			instance = Task01.class.newInstance();
			instance.capture();
			
			try
			{
				instance.prog();
			} catch (Exception ex)
			{
				ex.printStackTrace();
			}
			
			instance.endCapture();
			instance.validate();
			
		} catch (Exception ex)
		{
			ex.printStackTrace();
		}
	}
	
	@Override
	protected void _capture()
	{
		buffer = new SystemOutCapture();
		buffer.capture();
	}
	
	@Override
	protected void _endCapture()
	{
		buffer.endCapture();
	}
	
	@Override
	protected void _validate()
	{
		String line = "";
		for (int i = 0; i < 30; i ++)
			line += "=";
		
		System.out.println(line);
		
		if (buffer.result().hashCode() == 180322920)
			System.out.println("Solution serves the correct result");
		else
			System.out.println("Solution serves an incorrect result");
	}
}
